# huaweicloud-solution-PRIM-frontend

## 简介

这里是 PRIM 的前端平台，支持云产品购买及管理、财务管理、订单管理、账号信息管理等功能，方便用户进行云产品的操作。

## 开发框架

Vue3 + vite + TypeScript

## 目录结构

```json
|-- index.html -- 根页面
|-- package.json -- npm工具的配置文件
|-- README.md --项目说明文档
|-- tsconfig.json --ts配置文件
|-- tsconfig.node.json --ts编译器配置文件
|-- vite.config.ts --vite配置文件
|-- env --环境变量配置文件
|-- public --公共资源文件
|-- src
    |-- App.vue --主组件
    |-- main.ts --程序入口文件
    |-- style.css --公共样式
    |-- vite-env.d.ts --vite声明文件
    |-- api  --http接口文件
    |-- assets --静态文件
    |-- components  --公共组件
    |-- i18n --中英文配置
    |-- pages --页面组件
    |-- router --路由配置
    |-- store --状态管理配置
    |-- types --ts声明文件
    |-- utils  --公共工具
    |-- tips --参数说明文件
```

## 运行要求

- npm 8.1.2 及以上<br>
- @vue/cli 5.0.8 及以上<br>
- nodejs 16.13.1 及以上<br>

## 本地运行流程

1. 修改密码加密公钥配置文件 src/api/crypto.ts
   ```
   export function encryptedData(data: string) {
        const encryptor = new JSEncrypt()
        let key = `XXXXXXXXX` // 设置公钥
        encryptor.setPublicKey(key)
        return encryptor.encrypt(data).toString()
    }
   ```
2. 在根目录下运行 npm install 命令，安装项目所需要的依赖
   ```
   npm install
   ```
3. 在根目录下安装 vue 国际化插件
   ```
   npm install --save vue-i18n@next
   ```
4. 在根目录下运行 npm run dev 命令，运行项目
   ```
   npm run dev
   ```

## 部署流程

1. 在本地根目录下运行 npm run build 命令，将项目编译打包至根目录的 dist 文件夹下。
   ```
   npm run build
   ```
2. 准备一台远程服务器，用以放置前端程序，以及前端与后端信息交互。
3. 在服务器上安装 nginx，并替换 nginx/conf/nginx.conf 为以下配置文件（需补充后端服务器 IP）。
4. 把 dist 目录下的所有文件都复制到 nginx 网站根目录 nginx/html 下。
5. 在 nginx/sbin 目录下运行./nginx 命令，启动 nginx
   ```
   ./nginx
   ```

### nginx 配置文件 nginx.conf

    worker_processes  1;

    events {
        worker_connections  1024;
    }

    http {
        include       mime.types;
        default_type  application/octet-stream;
        sendfile        on;
        keepalive_timeout  300;
        client_max_body_size 6g;
        server {
            listen       80;
            server_name  localhost;

            location /api/ {
                # 修改以下IP为后端服务器IP，把 /api 路径下的请求转发给真正的后端服务器
                proxy_pass https://127.0.0.1:8080/;
            }

            location / {
                    root html;
                    try_files $uri /index.html;  # try_files：检查文件； $uri：监测的文件路径； /index.html：文件不存在重定向的新路径
                    index index.html;
            }

            error_page   500 502 503 504  /50x.html;
            location = /50x.html {
                root   html;
            }
        }
    }
