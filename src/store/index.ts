import { publicStore } from './public'
import { userStore } from './user'

export interface IAppStore {
  userStore: ReturnType<typeof userStore>
  publicStore: ReturnType<typeof publicStore>
}

const appStore: IAppStore = {} as IAppStore

/**
 * 注册app状态库
 */
export const registerStore = () => {
  appStore.userStore = userStore()
  appStore.publicStore = publicStore()
}
export default appStore
