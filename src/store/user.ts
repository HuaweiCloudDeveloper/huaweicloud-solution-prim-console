import { ref } from 'vue'
import { defineStore } from 'pinia'

// 使用setup模式定义
export const userStore = defineStore('userSetup', () => {
  // 用户名
  const name = ref<string>('')
  const UserType = ref<string>('')
  const isLogin = ref<boolean>(false)
  const userId = ref<string>('')

  function updateName(str: string) {
    name.value = str
  }
  function updateUserType(role: string) {
    UserType.value = role
  }

  function updateLogin(is: boolean) {
    isLogin.value = is
  }

  function updateUserId(str: string) {
    userId.value = str
  }
  return {
    name,
    updateName,
    isLogin,
    updateLogin,
    UserType,
    updateUserType,
    userId,
    updateUserId
  }
})
