import { reactive, ref } from 'vue'
import { defineStore } from 'pinia'
import { BreadcrumbData, Production } from '@/types/public'
import { RouteRecordRaw } from 'vue-router'

export const publicStore = defineStore('publicSetup', () => {
  // 菜单收起控制
  let isCollapse = ref(false)
  // 面包屑数据
  let breadcrumbData: Array<BreadcrumbData> = reactive([])
  // 同步角色菜单数据
  let menuData: Array<RouteRecordRaw> = reactive([])

  function updateIsCollapse() {
    isCollapse.value = !isCollapse.value
  }

  function updateBreadcrum(breadcrumArr: Array<BreadcrumbData>) {
    breadcrumbData.length = 0
    breadcrumbData.push(...breadcrumArr)
  }

  function updateMenuData(menuArr: any) {
    menuData.length = 0
    menuData.push(...menuArr)
  }

  return {
    isCollapse,
    updateIsCollapse,
    breadcrumbData,
    updateBreadcrum,
    menuData,
    updateMenuData
  }
})
