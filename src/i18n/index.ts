import { createI18n } from 'vue-i18n'
import EN from './en/en'
import CN from './ZH-CN/cn'
const messages = {
  cn: {
    ...CN
  },
  en: {
    ...EN
  }
}

const i18n = createI18n({
  locale: 'cn',
  globalInjection: true,
  fallbackLocale: 'en',
  silentTranslationWarn: true,
  legacy: false,
  messages
})

export default i18n
