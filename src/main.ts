import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import router from './router'
import VueCookies from 'vue-cookies'
import 'element-plus/theme-chalk/src/message-box.scss'
import 'element-plus/theme-chalk/src/message.scss'
import { createPinia } from 'pinia'
import { registerStore } from '@/store'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import i18n from './i18n/index'
import 'virtual:svg-icons-register'
import './utils/permissions'

const app = createApp(App)
app.provide('VueCookies', VueCookies)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}

const pinia = createPinia()
pinia.use(({ store }) => {
  const initialState = JSON.parse(JSON.stringify(store.$state))
  store.$reset = () => {
    store.$patch(initialState)
  }
})
app.use(pinia).use(router).use(i18n)

registerStore()
app.mount('#app')
