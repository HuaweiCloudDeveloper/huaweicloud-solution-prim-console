export const timeFormat = function (row: any, column: any, cellValue: any, index: any) {
  let date = new Date(cellValue)
  let year = date.getFullYear(),
    month = date.getMonth() + 1,
    day = date.getDate(),
    hours = date.getHours(),
    minutes = date.getMinutes(),
    seconds = date.getSeconds()
  let newTime = `${year}-${addNil(month)}-${addNil(day)} ${addNil(hours)}:${addNil(minutes)}:${addNil(seconds)}`
  return newTime
}
export const timeFormatFnc = function (cellValue: any) {
  let date = new Date(cellValue)
  let year = date.getFullYear(),
    month = date.getMonth() + 1,
    day = date.getDate()
  let newTime = `${year}-${addNil(month)}-${addNil(day)}`
  return newTime
}
export const timeFormathms = function (value: any) {
  let date = new Date(value)
  let year = date.getFullYear(),
    month = date.getMonth() + 1,
    day = date.getDate(),
    hours = date.getHours(),
    minutes = date.getMinutes(),
    seconds = date.getSeconds()
  let newTime = `${year}-${addNil(month)}-${addNil(day)} ${addNil(hours)}:${addNil(minutes)}:${addNil(seconds)}`
  return newTime
}
export const timeFormatPoint = function (value: string | number | Date) {
  let date = new Date(value)
  let year = date.getFullYear(),
    month = date.getMonth() + 1,
    day = date.getDate()
  let newTime = `${year}.${addNil(month)}.${addNil(day)}`
  return newTime
}

export const timeFormatUTC = function (row: any, column: any, cellValue: any, index: any) {
  let date = new Date(cellValue)
  let year = date.getUTCFullYear(),
    month = date.getUTCMonth() + 1,
    day = date.getUTCDate(),
    hours = date.getUTCHours(),
    minutes = date.getUTCMinutes(),
    seconds = date.getUTCSeconds(),
    zone = date.getTimezoneOffset()
  let zoneTime = getZone(zone)
  let newTime = `${year}-${addNil(month)}-${addNil(day)} ${addNil(hours)}:${addNil(minutes)}:${addNil(seconds)} ` + zoneTime
  return newTime
}
export const timeFormathmsUTC = function (value: any) {
  let date = new Date(value)
  let year = date.getUTCFullYear(),
    month = date.getUTCMonth() + 1,
    day = date.getUTCDate(),
    hours = date.getUTCHours(),
    minutes = date.getUTCMinutes(),
    seconds = date.getUTCSeconds(),
    zone = date.getTimezoneOffset()
  let zoneTime = getZone(zone)
  let newTime = `${year}-${addNil(month)}-${addNil(day)} ${addNil(hours)}:${addNil(minutes)}:${addNil(seconds)} ` + zoneTime
  return newTime
}
export const addNil = function (str: number) {
  let num = Number(str)

  if (num < 10) {
    return `0${num}`
  } else {
    return str
  }
}
export const getZone = function (str: number) {
  let t: string
  if (str <= 0) {
    t = 'GMT+'
    str = -str
  } else {
    t = 'GMT-'
  }
  let num = Number(str)
  let h = addNil(num / 60)
  let m = addNil(num % 60)
  t = t + h + ':' + m
  return t
}
