/**
 * 系统角色权限，针对routes生成动态路由及菜单
 *
 */

 export function getUserMenuData() {
  const user = [
    'Homepage',
    'ProductsServices',
    'Products',
    'FinancialManagement',
    'OrderManagement',
    'AccountManagement',
    'Notify',
    'onDemanddetail',
    'BecuritySetting',
    'ChangePassword'
  ]
  const admin = ['Homepage', 'first', 'UserAdmin']

  return localStorage.getItem('role') === 'user' ? user : admin
}
