import mittjs, { Emitter } from 'mitt'

type Events = {
  [propName: string]: any
}

const mitt: Emitter<Events> = mittjs<Events>()
export default mitt
