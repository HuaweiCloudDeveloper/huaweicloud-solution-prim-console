/**
 * 对象深拷贝
 */

function IsArray(obj: any) {
  return obj && typeof obj === 'object' && obj instanceof Array
}

export const deepClone = function <T>(tSource: T, tTarget?: Record<string, any> | T): T {
  if (IsArray(tSource)) {
    tTarget = tTarget || []
  } else {
    tTarget = tTarget || {}
  }

  for (const key in tSource) {
    if (Object.prototype.hasOwnProperty.call(tSource, key)) {
      if (typeof tSource[key] === 'object' && typeof tSource[key] !== null) {
        tTarget[key] = IsArray(tSource[key]) ? [] : {}
        deepClone(tSource[key], tTarget[key])
      } else {
        tTarget[key] = tSource[key]
      }
    }
  }

  return tTarget as T
}

/**
 * 千分位分隔符
 * @param value
 * @returns
 */

export function numberToCurrencyNo(value: number) {
  if (!value) return 0
  // 获取整数部分
  const intPart = Math.trunc(value)
  // 整数部分处理，增加,
  const intPartFormat = intPart.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,')
  // 预定义小数部分
  let floatPart = ''
  // 将数值截取为小数部分和整数部分
  const valueArray = value.toString().split('.')
  if (valueArray.length === 2) {
    // 有小数部分
    floatPart = valueArray[1].toString() // 取得小数部分
    return intPartFormat + '.' + floatPart
  }
  return intPartFormat + floatPart
}

/**
 * 正则匹配
 */
// 字符串存在至少一个字符
function pantterSpecialCharacters(str: string) {
  return /[-~#（）|【-】· (){}+=*^&%$@!.,，。<>;:：；‘’“”、'?`]+/.test(str)
}
// 至少存在一个大写字母
function pantterUpperCaseCharacters(str: string) {
  return /[A-Z]+/.test(str)
}
function pantterLowerCaseCharacters(str: string) {
  return /[a-z]+/.test(str)
}

function pantterNumber(str: string) {
  return /[0-9]+/.test(str)
}
// 密码匹配
export function passwordCheck(str: string) {
  return pantterLowerCaseCharacters(str) && pantterUpperCaseCharacters(str) && pantterSpecialCharacters(str) && pantterNumber(str)
}

/**
 * 日期选择限制天数
 *
 */
class DateLimt {
  chooseDay: any
  constructor() {
    this.chooseDay = ref<any>(null)
  }

  handleChange = (val: Date[]) => {
    const [pointDay] = val
    this.chooseDay.value = pointDay
  }
  handleFocus = () => {
    this.chooseDay.value = null
  }
  disabledDate = (time: number) => {
    if (!this.chooseDay.value) {
      return false
    }

    let timeRange = 31
    const con1 = new Date(this.chooseDay.value).getTime() - timeRange * 24 * 60 * 60 * 1000
    const con2 = new Date(this.chooseDay.value).getTime() + timeRange * 24 * 60 * 60 * 1000
    return time < con1 || time > con2
  }
}

export default new DateLimt()
