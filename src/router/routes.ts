import { RouteRecordRaw } from 'vue-router'

/**
 * isNotShow: 是否展示在左侧菜单
 * meta: {
 *    title: 标题或者菜单名称
 *    isNotShowLeft：是否展示左侧菜单
 *    icon: 菜单图标
 * }
 *
 * 注意：路由加完需要在utils/authentication.ts里加权限，否则不显示
 *
 */

export default [
  {
    path: '/',
    name: 'Homepage',
    isNotShow: true,
    component: () => import('@/pages/homepage.vue')
  },
  {
    path: '/first',
    name: 'first',
    isNotShow: true,
    meta: {
      isNotShowLeft: true
    },
    component: () => import('@/pages/firstTime.vue')
  },
  {
    path: '/changePassword',
    name: 'ChangePassword',
    meta: {
      isNotShowLeft: true
    },
    component: () => import('@/pages/user/changePassword.vue')
  },
  {
    path: '/productsServices',
    name: 'ProductsServices',
    component: () => import('@/pages/productsServices.vue'),
    meta: {
      title: '产品与服务',
      icon: 'ProductsServices'
    },
    beforeEnter: (to, from, next) => {
      // next(from.path)
    }
  },
  {
    path: '/products',
    name: 'Products',
    component: () => import('@/pages/products.vue'),
    isNotShow: true,
    meta: {
      title: '产品列表'
    }
  },
  {
    path: '/financialManagement',
    name: 'FinancialManagement',
    redirect: 'financialManagement/voucherCenter',
    meta: {
      icon: 'financialManagement',
      title: '财务管理'
    },
    children: [
      {
        name: 'VoucherCenter',
        path: 'voucherCenter',
        component: () => import('@/pages/financialManagement/voucherCenter.vue'),
        meta: {
          title: '充值中心'
        },
        beforeEnter: (to, from, next) => {
          if (sessionStorage.getItem('AUTHEN') === '0') {
            ElMessage.warning('未完成认证，暂无法访问！')
          } else {
            next()
          }
        }
      },
      {
        name: 'RevenueExpenditureDetails',
        path: 'revenueExpenditureDetails',
        component: () => import('@/pages/financialManagement/revenueExpenditureDetails.vue'),
        meta: {
          title: '收支明细'
        }
      }
    ]
  },
  {
    path: '/orderManagement',
    name: 'OrderManagement',
    redirect: 'orderManagement/OrderToBePaid',
    meta: {
      icon: 'orderManagement',
      title: '订单管理'
    },
    children: [
      {
        name: 'OrderToBePaid',
        path: 'orderToBePaid',
        component: () => import('@/pages/orderManagement/orderToBePaid.vue'),
        meta: {
          title: '待支付订单'
        }
      },
      {
        name: 'MyOrder',
        path: 'myorder',
        component: () => import('@/pages/orderManagement/myOrder.vue'),
        meta: {
          title: '我的订单'
        }
      },
      {
        path: 'onDemanddetail',
        name: 'onDemanddetail',
        isNotShow: true,
        component: () => import('@/pages/orderManagement/onDemanddetail.vue')
      },
      {
        path: 'cycleFinishedorder',
        name: 'cycleFinishedorder',
        isNotShow: true,
        component: () => import('@/pages/orderManagement/cycleFinishedorder.vue')
      },
      {
        path: 'cycleTobepaidorder',
        name: 'cycleTobepaidorder',
        isNotShow: true,
        component: () => import('@/pages/orderManagement/cycleTobepaidorder.vue')
      },
      {
        path: 'cycleOrders',
        name: 'CycleOrders',
        isNotShow: true,
        component: () => import('@/pages/orderManagement/cycleOrders.vue')
      }
    ]
  },
  {
    path: '/accountManagement',
    name: 'AccountManagement',
    redirect: 'accountManagement/pertificationInformation',
    meta: {
      icon: 'accountManagement',
      title: '账号管理'
    },
    children: [
      {
        name: 'PertificationInformation',
        path: 'pertificationInformation',
        component: () => import('@/pages/user/accountManagement/pertificationInformation.vue'),
        meta: {
          title: '认证信息'
        }
      },
      {
        name: 'BasicInformation',
        path: 'basicInformation',
        component: () => import('@/pages/user/accountManagement/basicInformation.vue'),
        meta: {
          title: '基本信息'
        }
      },
      {
        name: 'BecuritySetting',
        path: 'securitySetting',
        component: () => import('@/pages/user/accountManagement/securitySetting.vue'),
        meta: {
          title: '安全设置'
        }
      }
    ]
  },
  {
    path: '/v1/user/order/notify',
    name: 'Notify',
    isNotShow: true,
    meta: {
      isNotShowLeft: true
    },
    component: () => import('@/components/notify.vue')
  },

  {
    path: '/userAdmin',
    name: 'UserAdmin',
    redirect: 'userAdmin/userList',
    meta: {
      icon: 'userAdmin',
      title: '用户管理'
    },
    children: [
      {
        path: 'userList',
        name: 'UserList',
        component: () => import('@/adminPages/userAdmin/userList.vue'),
        meta: {
          title: '用户列表'
        }
      }
    ]
  }
] as Array<RouteRecordRaw>
