/**
 * 路由白名单
 */

import { RouteRecordRaw } from 'vue-router'

export default [
  {
    path: '/login',
    name: 'Login',
    meta: {
      isNotShowLeft: true
    },
    component: () => import('@/pages/login.vue')
  },
  {
    path: '/register',
    name: 'Register',
    meta: {
      isNotShowLeft: true
    },
    component: () => import('@/pages/user/Register.vue')
  },
  {
    path: '/samlRequestFirst',
    name: 'samlRequestFirst',
    meta: {
      isNotShowLeft: true
    },
    component: () => import('@/components/samlRequestFirst.vue'),
    props: (route) => ({ samlResponse: route.query.SAMLResponse })
  }
] as Array<RouteRecordRaw>
