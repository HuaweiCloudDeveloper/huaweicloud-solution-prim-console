import { AxiosResponse } from 'axios'

interface TabMenuItem {
  id: string
  text: string
}

interface User {
  username: string
}

interface MenuData {
  title: string
  path: string
  name: string
  icon: string
  child: Array<MenuData>
}

interface BreadcrumbData {
  title: string
  path?: string
}

interface MyResponse extends AxiosResponse {
  code: number
}

interface Production {
  iframeURL: string
  price: number
  productionId: string
  productionName: string
  productionType: string
  titleName?: string
}

interface QueryParams {
  page?: number
  pageSize?: number
}
