export const paymentMethodArrs = [
  {
    title: '支付工具',
    child: [
      {
        id: '1',
        text: '支付宝',
        value: 1
      },
      {
        id: '2',
        text: '微信',
        value: 2
      }
    ]
  }
  // {
  //   title: '银行卡支付',
  //   child: [
  //     {
  //       id: '3',
  //       text: '中国银行',
  //       value: 3
  //     }
  //   ]
  // }
]

// 付费模式
export const chargeMode = [
  {
    id: '1',
    text: '包周期',
    value: 1
  },
  {
    id: '3',
    text: '按需',
    value: 3
  },
  {
    id: '10',
    text: '预留示例',
    value: 10
  }
]

// 账单支付状态
export const billStatusData = [
  {
    id: '1',
    text: '已支付',
    value: 1
  },
  {
    id: '2',
    text: '未结清',
    value: 2
  },
  {
    id: '3',
    text: '未结算',
    value: 3
  }
]
// 账单类型
export const billTypeData = [
  {
    text: '消费-新购',
    value: 1
  },
  {
    text: '消费-续订',
    value: 2
  },
  {
    text: '消费-变更',
    value: 3
  },
  {
    text: '消费-退订',
    value: 4
  },
  {
    text: '消费-使用',
    value: 5
  },
  {
    text: '消费-自动续订',
    value: 8
  },
  {
    text: '调账-补偿',
    value: 9
  },
  {
    text: '消费-服务支持计划月末扣费',
    value: 14
  },
  {
    text: '调账-扣费',
    value: 16
  },
  {
    text: '消费-按月付费',
    value: 18
  },
  {
    text: '退款-变更',
    value: 20
  }
]
