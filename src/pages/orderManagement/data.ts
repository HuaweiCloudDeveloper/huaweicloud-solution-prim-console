export const getorderType = [
  {
    title: '订单类型',
    child: [
      {
        id: '1',
        text: '开通',
            value: 1
        },
      {
        id: '2',
        text: '续订',
        value: 2
      },
      {
        id: '3',
        text: '变更',
        value: 3
        },
       {
        id: '4',
        text: '退订',
        value: 4
        },
        {
        id: '11',
        text: '按需转包年/包月',
        value: 11
        },
         {
        id: '13',
        text: '试用',
        value: 13
        },
          {
        id: '14',
        text: '转商用',
        value: 14
        },
           {
        id: '15',
        text: '费用调整',
        value: 15
      },
    ]
        }]
    
export const getorderStatus = [
  {
    title: '订单状态',
    child: [
      {
        id: '1',
        text: '待审核',
        value: 1,
        color:'#FF7500'
        },
        {
        id: '2',
        text: '待退款',
        value: 2,
        color:'#FF7500'
        },
        {
        id: '3',
        text: '处理中',
          value: 3,
        color:'#FF7500'
        },
        {
        id: '4',
        text: '已取消',
        value: 4,
        color:'#C6C9D3'
        },
        {
        id: '5',
        text: '已完成',
        value: 5,
        color: '#00C6b4'
        },
        {
        id: '6',
        text: '待支付',
          value: 6,
        color:'#FF7500'
        },
        {
        id: '9',
        text: '待确认',
          value: 9,
        color:'#FF7500'
        },
        {
        id: '10',
        text: '待发货',
          value: 10,
        color:'#FF7500'
        },
        {
        id: '11',
        text: '待收货',
          value: 11,
        color:'#FF7500'
        },
        {
        id: '12',
        text: '待上门取货',
          value: 12,
        color:'#FF7500'
        },
        {
        id: '13',
        text: '换新中',
          value: 13,
        color:'#FF7500'
      }
      
    ]
    }]