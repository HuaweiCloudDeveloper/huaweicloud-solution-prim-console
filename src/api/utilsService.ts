import Service from '@/api/httpAxios'
import { MyResponse } from '@/types/public'

class Utils {
  // 图片上传
  public uploadFile(data: any) {
    return Service.post('v1/file/upload', data)
  }
}

export default new Utils()
