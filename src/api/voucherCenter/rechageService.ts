import Service from '@/api/httpAxios'
class Rechage {
  // 余额查询
  public balanceQuery = (params: any) => {
    const url = `v1/user/balance/query`
    return Service.get<any>(url, params)
  }
  // 充值记录
  public balanceChangeRecord = (params: any) => {
    const url = `v1/user/balance/changeRecord`
    return Service.post<any>(url, params)
  }
  // 收支明细
  public billQuery = (data: any) => {
    return Service.post<any>('v1/user/bill/query', data)
  }
}

export default new Rechage()
