import Service from '@/api/httpAxios'

class OrderManagement{

    //查询包周期订单
    public getOnePay = (params:any)=>{
        const url = '/v1/user/order/query/onePay'
        return Service.post<any>(url, params)
    }

    //查询按需订单
    public getonDemand = (params:any)=>{
        const url = '/v1/user/order/query/onDemand'
        return Service.post<any>(url, params)
    }


}

export default new OrderManagement()





