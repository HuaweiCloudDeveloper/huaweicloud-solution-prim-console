import Service from '@/api/httpAxios'
class Production {
  public productionList = (params: any) => {
    const url = `v1/production/list`
    return Service.get<any>(url, params)
  }
}

export default new Production()
