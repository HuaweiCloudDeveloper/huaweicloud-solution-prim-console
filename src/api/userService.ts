import Service from '@/api/httpAxios'
import { MyResponse } from '@/types/public'

type loginType = {
  [x: string]: any
  token: string
}
type resconfig = {
  resconfig?: Object
  id?: string
  offset?: number
  limit?: number
}

class UserService {
  // 验证码
  public getCaptcha = () => {
    return Service.get<any>('v1/user/captcha')
  }
  // 登录
  public login = (username: string, password: string, captcha: string) => {
    return Service.post<loginType>('v1/user/login', {
      userName: username,
      password,
      captcha
    })
  }
  // 登出
  public logout = (data: Object) => {
    return Service.post<MyResponse>('v1/user/logout', data)
  }

  public getSaml = (sessionId: any) => {
    const url = `/v1/saml/get`
    return Service.get<any>(url, { sid: sessionId })
  }

  public samlLogin = (RelayState: string, SAMLRequest: string, SigAlg: string, Signature: string, userId: string, token: string) => {
    const url = `/v1/saml/login`
    return Service.post<any>(url, { RelayState, SAMLRequest, SigAlg, Signature, userId, token })
  }

  public changePass = (obj: resconfig) => {
    const url = `/v1/user/changePwd`
    return Service.post<any>(url, obj.resconfig)
  }

  public profile = (params: any) => {
    const url = `/v1/user/profile`
    return Service.get<any>(url, params)
  }

  // public delUser = (id: string) => {
  //   const url = `/v1/admin/userinfo?id=${id}`
  //   return Service.delete<any>(url)
  // }

  public registe = (username: string, password: string, captcha: string, phoneNumber?: string, phoneVerificationCode?: string) => {
    return Service.post<loginType>('v1/user/registration', {
      username,
      password,
      captcha,
      phoneNumber,
      phoneVerificationCode
    })
  }

  // 实名申请
  public userAuthentication(data: any) {
    return Service.post('v1/user/authentication', data)
  }

  // 检测是否实名
  public checkauthentication(params: any) {
    return Service.get('v1/user/checkauthentication', params)
  }

  //支付
  public goPay = (userId: string, orderId: string) => {
    const url = 'v1/user/order/pay'
    return Service.post(url, { userId, orderId })
  }

  //退订
  public cancel = (userId: string, orderId: string) => {
    const url = 'v1/user/order/cancel'
    return Service.put(url, { userId, orderId })
  }

  // 获取图像
  public getProfilePhoto = (params: any) => {
    return Service.get('v1/user/getProfilePhoto', params)
  }
}

export default new UserService()
